﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            WebRequest request = WebRequest.Create("https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,DASH&tsyms=USD");
            WebResponse response = request.GetResponse();
//            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            Stream dataStream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(dataStream);

            var json = streamReader.ReadToEnd(); ;
            var objects = JObject.Parse(json.ToString());
            Console.WriteLine("Current BTC course: " + objects.GetValue("BTC").Value<string>("USD"));
            
            streamReader.Close();
            response.Close();
            Console.ReadLine();
        }
    }
}
