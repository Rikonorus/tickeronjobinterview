﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please, input cryptocurrency ticker");
            String ticker = Console.ReadLine();
            
            WebRequest request = WebRequest.Create("https://min-api.cryptocompare.com/data/pricemulti?fsyms=" + ticker + "&tsyms=USD");
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(dataStream);

            var json = streamReader.ReadToEnd(); 
            var objects = JObject.Parse(json.ToString());
            if (objects.GetValue("Response") != null && objects.GetValue("Response").Value<string>() == "Error")
            {
                Console.WriteLine(objects.GetValue("Message").Value<string>());
            }
            else
            {
                Console.WriteLine("Current " + ticker + " course: " + objects.GetValue(ticker).Value<string>("USD"));
            }

            streamReader.Close();
            response.Close();
            Console.ReadLine();
        }
    }
}
