﻿using System;
using System.Net;
using System.IO;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            var doc = new HtmlDocument();
            var html = HtmlNode.CreateNode("<html><head></head><body></body></html>");
            doc.DocumentNode.AppendChild(html);
            var body = doc.DocumentNode.SelectSingleNode("/html/body");
            var table = HtmlNode.CreateNode("<table><thead><th>Currency</th><th>Price</th></thead><tbody></tbody></table>");
            body.AppendChild(table);

            WebRequest request = WebRequest.Create("https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,DASH&tsyms=USD");
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(dataStream);
            var json = streamReader.ReadToEnd();
            var objects = JObject.Parse(json.ToString());

            var tbody = table.SelectSingleNode("/table/tbody");

            foreach (var x in objects)
            {
                string ticker = x.Key;
                JToken price = x.Value.Value<string>("USD");
                tbody.AppendChild(HtmlNode.CreateNode("<tr><td>" + ticker + "</td><td>" + price + "</td></tr>"));
                
            }
          
            System.IO.File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\index.html", html.OuterHtml);
            Console.WriteLine("File successfully created by path: " + Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\index.html");
            Console.ReadLine();

        }
    }
}
