﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            WebRequest request = WebRequest.Create("https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,DASH&tsyms=USD");
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(dataStream);

            var json = streamReader.ReadToEnd();
            var objects = JObject.Parse(json.ToString());
            foreach (var x in objects)
            {
                string ticker = x.Key;
                JToken price = x.Value.Value<string>("USD");
                Console.WriteLine("Current " + ticker + " course: " + price);
            }

            streamReader.Close();
            response.Close();
            Console.ReadLine();
        }
    }
}
