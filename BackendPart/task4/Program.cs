﻿using System;
using System.Net;
using System.IO;

namespace task4
{
    class Program
    {
        static void Main(string[] args)
        {
            WebRequest request = WebRequest.Create("https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,DASH&tsyms=USD");
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(dataStream);

            var json = streamReader.ReadToEnd();
            System.IO.File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\json.json", json);
            Console.WriteLine("File successfully created by path: " + Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\json.json");
            Console.ReadLine();
        }
    }
}
